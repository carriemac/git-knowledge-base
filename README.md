# My Git knowledge base

Commands and resources for a Git novice.

## How to

### Fork the repository to my GitLab namespace

1. In GitLab, go to the repository
1. Select **Fork** at the top of the page
1. Fill out the form
1. Select **Fork project** at the bottom of the page

### Get the SSH URL
1. Stay in the GitLab repository
1. Select **Clone**
1. Copy the URL for **Clone with SSH**

### Clone to my local computer and set up credentials

1. Go to the command-line interface (CLI)
1. `mkdir DIRECTORY-NAME`
1. `cd DIRECTORY-NAME`
1. `git clone FORK-NAME` [for FORK-NAME, insert the copy-pasted SSH URL]
1. `git config user.name "NAME"`
1. `git config user.email "EMAIL"`

### Pull upstream changes to my local computer

1. Stay in the CLI
1. `git remote -v` [shows URLs of remote (aka upstream) repositories]
1. `git remote add upstream UPSTREAM-REPOSITORY` [for UPSTREAM-REPOSITORY, insert the URL of the repository in your GitLab namespace]
1. `git remote -v`

### Check out a new branch on my local computer

1. Go to the CLI
1. `git checkout BRANCH-NAME` [moves to the branch you've specified;g start with MAIN]
1. `? git pull` [gets the latest updates]
1. `git checkout -b BRANCH-NAME` [gives the new branch a name and switches to it]

### Revise a document on the branch

1. Go to vscode
1. File > Open > `FILENAME`

### Stage and commit revisions, then push revisions upstream

1. `git status` [shows what's been changed]
1. `git diff` [shows changes line-by-line]
1. `git add .` [stages everything]
1. `git commit -m "DESCRIPTION"`
1. `git push`

### Work on a branch that already exists locally

1. `git branch` [show what branches exist]
1. `git checkout BRANCH-NAME` [switches to a branch]

## Resources

* [Project Forking Workflow | GitLab](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)
* [Clone a repository | GitLab](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository)
* [Git Cheat Sheet | GitLab](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
* [Git Documentation](https://git-scm.com/doc)
* [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/)